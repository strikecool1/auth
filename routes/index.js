const express = require('express');
const jwt = require("jsonwebtoken")
const router = express.Router();
// Ключ JWT
const jwtsecret = "javainuse-secret-key"

// рендер страницы auth
router.get('/', 
  function(req, res) {
    jwt.verify(req.cookies.cookieToken, jwtsecret, function (err, decoded) {
      if (err) {
        res.render('pages/auth', { title: 'Авторизация' });
      } else {
        res.render("pages/main", { name: decoded.name });
      }
    })
});

// рендер страницы main
router.get('/home',mustBeLoggedIn,
  function(req, res) {
    res.render('pages/main', { title: 'Главная страница' });
});

// обработчик верификации пользователя
router.post("/login", (req, res) => {
  if (req.body.username === "vitalik" && req.body.password === "qwerty") {
    res.cookie("cookieToken", jwt.sign({ name: req.body.username, role: "1" }, jwtsecret), { httpOnly: true })
    res.redirect("/")
  } else {
    res.send({message:"Неверный логин или пароль"});
  }
});


// промежуточная проверка токена
function mustBeLoggedIn(req, res, next) {
  jwt.verify(req.cookies.cookieToken, jwtsecret, 
    function (err, decoded) {
    if (err) {
      res.redirect("/");
    } else {
      next();
    }
  });
}


router.get("/logout", (req, res) => {
  res.clearCookie("cookieToken")
  res.redirect("/");
});

module.exports = router;
